# Emacs & Cask Docker Image

This repository generates a Docker image with Emacs, Cask, and basic
development tools installed. It is useful for testing Emacs packages.


# Use

In your GitLab CI file, declare

```yaml
image: "registry.gitlab.com/joewreschnig/emacs-cask-docker-image:alpine-edge"
```

For an example, see [`gitlab-ci-mode`’s `gitlab-ci.yml`][use].


# License

This is free and unencumbered software released into the public domain.
Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any means.


[use]: https://gitlab.com/joewreschnig/gitlab-ci-mode/blob/master/.gitlab-ci.yml
